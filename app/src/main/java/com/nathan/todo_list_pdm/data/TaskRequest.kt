package com.nathan.todo_list_pdm.data

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.android.volley.Request
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.nathan.todo_list_pdm.model.Task
import org.json.JSONArray
import org.json.JSONObject

class TaskRequest (context: Context) {
    private val queue = Volley.newRequestQueue(context)

    companion object {
        private val URL = "http://10.0.2.2:5000"
        private val GET_TASKS = "/tasks"
        private val POST_TASK = "/tasks/new"
        private val UPDATE_TASK = "/tasks/done"
        private val REMOVE_TASK = "/tasks/del"
    }

    fun startTasksRequest() {
        val handler = Handler(Looper.getMainLooper())
        handler.post(object : Runnable {
            override fun run() {
                tasksRequest()
                handler.postDelayed(this, 5000)
            }
        })
    }

    fun tasksRequest() {
        val jsonRequest = JsonArrayRequest(
            Request.Method.GET,
            URL + GET_TASKS,
            null,
            { response ->
                val taskList = JSONArrayToTaskList(response)
                TasksSingleton.updateTasksList(taskList)
            },
            { error ->
                Log.e("TASKREQERR", "Task request error: ${error}")
            }
        )
        this.queue.add(jsonRequest)
    }

    fun taskAddRequest(task: Task){
        val requestBody = TaskToJSONObject(task)
        val jsonRequest = JsonObjectRequest(
            Request.Method.POST,
            URL + POST_TASK,
            requestBody,
            {
                tasksRequest()
                Log.v("TASKPOST", "Task posted: $requestBody")
            },
            {  error ->
                Log.e("TASKPOSTERR", "Task post error: $error")
            }
        )
        this.queue.add(jsonRequest)
    }

    fun taskUpdateRequest(task: Task){
        val url = URL + UPDATE_TASK + "/${task.isDone}/${task.id}"
        var strRequest = StringRequest(
            Request.Method.PUT,
            url,
            {
                tasksRequest()
                Log.v("TASKUPDATE", "Task updated id: ${task.id}")
            },
            {
                error -> Log.e("TASKUPDATEERR", "Task update error: $error")
            }
        )
        this.queue.add(strRequest)
    }

    fun taskRemoveRequest(task: Task){
        val url = URL + REMOVE_TASK + "/${task.id}"
        var strRequest = StringRequest(
            Request.Method.DELETE,
            url,
            {
                tasksRequest()
                Log.v("TASKRMV","Task removed id: ${task.id}")
            },
            {
                error -> Log.e("TASKRMVERR","Task remove error: $error")
            }
        )
        this.queue.add(strRequest)
    }

    private fun JSONArrayToTaskList(jsonArray: JSONArray): ArrayList<Task> {
        val taskList = ArrayList<Task>()
        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObject = jsonArray.getJSONObject(i)
            val task = Task(
                jsonObject.getString("content"),
                jsonObject.getBoolean("isUrgent"),
                jsonObject.getBoolean("isDone"),
                jsonObject.getLong("id"),
            )
            taskList.add(task)
        }
        return taskList
    }

    private fun TaskToJSONObject(task: Task): JSONObject{
        val jsonObject = JSONObject()
        jsonObject.put("content", task.content)
        jsonObject.put("isUrgent", task.isUrgent)
        jsonObject.put("isDone", task.isDone)
        return jsonObject
    }
}