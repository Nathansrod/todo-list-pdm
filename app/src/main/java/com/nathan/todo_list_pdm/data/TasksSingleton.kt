package com.nathan.todo_list_pdm.data

import androidx.compose.runtime.mutableStateListOf
import com.nathan.todo_list_pdm.model.Task

object TasksSingleton{
    private val tasks = mutableStateListOf<Task>()
    fun updateTasksList(tasks: ArrayList<Task>) {
        this.tasks.clear()
        this.tasks.addAll(tasks)
    }
    fun getTasks(): List<Task> {
        return this.tasks
    }
    fun removeTask(task: Task){
        this.tasks.remove(task)
    }
}