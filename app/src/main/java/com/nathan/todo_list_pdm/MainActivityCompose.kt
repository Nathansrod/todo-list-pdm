package com.nathan.todo_list_pdm

import android.os.Build
import android.os.Bundle
import android.view.WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS
import android.view.WindowManager
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.toArgb
import com.nathan.todo_list_pdm.data.TaskRequest
import com.nathan.todo_list_pdm.ui.screens.TaskListScreen
import com.nathan.todo_list_pdm.ui.theme.Todo_list_pdmTheme

class MainActivityCompose : ComponentActivity() {
    private lateinit var taskRequest: TaskRequest
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        this.taskRequest = TaskRequest(this)
        this.taskRequest.startTasksRequest()
        setContent {
            Todo_list_pdmTheme {
                this.SetupUIConfigs()
                TaskListScreen(this.taskRequest)
            }
        }
    }
    @Composable
    private fun SetupUIConfigs() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (MaterialTheme.colors.isLight) {
                window.decorView
                    .windowInsetsController?.setSystemBarsAppearance(
                        APPEARANCE_LIGHT_STATUS_BARS, APPEARANCE_LIGHT_STATUS_BARS
                    )
            } else {
                window.decorView
                    .windowInsetsController?.setSystemBarsAppearance(
                        0, APPEARANCE_LIGHT_STATUS_BARS
                    )
            }
        }
        window.statusBarColor = MaterialTheme.colors.primaryVariant.toArgb()
    }

}