package com.nathan.todo_list_pdm.model

data class Task (
    val content: String,
    val isUrgent: Boolean,
    var isDone: Boolean,
    var id: Long = 0,
)