package com.nathan.todo_list_pdm.ui.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nathan.todo_list_pdm.R
import com.nathan.todo_list_pdm.model.Task

class TaskAdapter (
    private var tasks: ArrayList<Task>
    ): RecyclerView.Adapter<TaskViewHolder>() {
    private var listener: OnClickDoneListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val layoutRes = if(viewType == 1) R.layout.cardview_task_urgent else R.layout.cardview_task_default
        val itemView = layoutInflater.inflate(layoutRes, parent, false)
        return TaskViewHolder(itemView, this)
    }

    fun interface OnClickDoneListener {
        fun OnClickDone(task: Task)
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.bind(this.tasks[position])
    }

    override fun getItemCount(): Int {
        return this.tasks.size
    }

    override fun getItemViewType(position: Int): Int {
        return if(this.tasks[position].isUrgent) 1 else 0
    }

    // metodos auxiliares
    fun setOnClickDoneListener(listener: OnClickDoneListener?) {
        this.listener = listener
    }
    fun getOnClickDoneListener(): OnClickDoneListener? {
        return this.listener
    }
}