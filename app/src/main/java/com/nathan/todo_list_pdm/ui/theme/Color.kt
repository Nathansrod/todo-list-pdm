package com.nathan.todo_list_pdm.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val BlackAccent = Color(0xFF151515)
val DarkGray = Color(0xFF292929)
val LightGray = Color(0xFF858585)
val WhiteAccent = Color(0xFFDDDDDD)
val Red = Color(0xFFB84040)
val Blue = Color(0xFF4E4EB9)
val Green = Color(0xFF40A040)
val DeepBlue = Color(0xFF0D2149)
val SkyBlue = Color(0xFFA1CDF4)



