package com.nathan.todo_list_pdm.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable

private val DarkColorPalette = darkColors(
    primary = BlackAccent,
    onPrimary = WhiteAccent,
    primaryVariant = LightGray,
    secondary = DarkGray,
    onSecondary = WhiteAccent,
)

private val LightColorPalette = lightColors(
    primary = WhiteAccent,
    onPrimary = BlackAccent,
    primaryVariant = DarkGray,
    secondary = LightGray,
    onSecondary = BlackAccent

    /* Other default colors to override
    background = Color.White,
    surface = Color.White,
    onPrimary = Color.White,
    onSecondary = Color.Black,
    onBackground = Color.Black,
    onSurface = Color.Black,
    */
)

@Composable
fun Todo_list_pdmTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}