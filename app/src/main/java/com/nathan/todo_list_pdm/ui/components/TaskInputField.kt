package com.nathan.todo_list_pdm.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.nathan.todo_list_pdm.data.TaskRequest
import com.nathan.todo_list_pdm.data.TasksSingleton
import com.nathan.todo_list_pdm.model.Task
import com.nathan.todo_list_pdm.ui.theme.Todo_list_pdmTheme

@Composable
fun TaskInputField(taskRequest: TaskRequest){
    var text by remember { mutableStateOf("") }
    var switchState by remember { mutableStateOf(false) }
    Surface(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 2.5.dp, vertical = 2.5.dp),
    ) {
        Column(
            modifier = Modifier.padding(5.dp)
        ){
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 15.dp, vertical = 2.5.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ){
                Text(text = "Urgente")
                Switch(
                    checked = switchState,
                    onCheckedChange = {switchState = !switchState},
                )
            }
            TextField(
                value = text,
                onValueChange = { text = it },
                label = { Text("Tarefa") },
                modifier = Modifier.fillMaxWidth()
            )
            Button(
                onClick = {
                    val newTask = Task(
                        text,
                        switchState,
                        false,
                    )
                    taskRequest.taskAddRequest(newTask)
                    text = ""
                    switchState = false
                },
                modifier = Modifier.fillMaxWidth(),
            ) {
                // Inner content including an icon and a text label
                Icon(
                    Icons.Filled.Add,
                    contentDescription = "Adicionar Tarefa",
                    modifier = Modifier.size(ButtonDefaults.IconSize)
                )
                Spacer(Modifier.size(ButtonDefaults.IconSpacing))
                Text("Adicionar Tarefa")
            }
        }
    }
}