package com.nathan.todo_list_pdm.ui.components

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.nathan.todo_list_pdm.data.TaskRequest
import com.nathan.todo_list_pdm.data.TasksSingleton
import com.nathan.todo_list_pdm.model.Task
import com.nathan.todo_list_pdm.ui.theme.Green
import com.nathan.todo_list_pdm.ui.theme.WhiteAccent

@Composable
fun TaskItemView(task: Task, taskRequest: TaskRequest){
    var expanded by remember { mutableStateOf(false)}
    var showAlertDialog by remember { mutableStateOf(false)}
    Surface(
        modifier = Modifier
            .pointerInput(Unit) {
                detectTapGestures(
                    onTap = {
                        expanded = !expanded
                    },
                    onLongPress = {
                        Log.d("CLICKEVNT", "Long Click on ${task.content}")
                        showAlertDialog = true
                        //TasksSingleton.removeTask(task)
                        //taskRequest.taskRemoveRequest(task)
                    }
                )
            }
    ){
        Card(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 2.5.dp, vertical = 2.5.dp),
            elevation = 5.dp,
        ){
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.height(IntrinsicSize.Max)
            ){
                Box(modifier = Modifier
                    .fillMaxHeight()
                    .width(20.dp)
                    .background(
                        if (task.isUrgent) Color.Red
                        else Color.Blue
                    ))
                if(expanded){
                    Text(
                        text = task.content,
                        color = MaterialTheme.colors.onPrimary,
                        style = MaterialTheme.typography.body1,
                        modifier = Modifier
                            .padding(horizontal = 10.dp, vertical = 10.dp)
                            .weight(1f)
                            .align(Alignment.CenterVertically),
                    )
                }
                else{
                    Text(
                        text = task.content,
                        color = MaterialTheme.colors.onPrimary,
                        style = MaterialTheme.typography.body1,
                        modifier = Modifier
                            .padding(horizontal = 10.dp, vertical = 10.dp)
                            .weight(1f)
                            .align(Alignment.CenterVertically),
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis,
                    )
                }
                Box(
                    modifier = Modifier
                        .fillMaxHeight()
                        .padding(10.dp)
                ){
                    DoneCheckbox(
                        task = task,
                        modifier = Modifier.align(Alignment.Center),
                        taskRequest = taskRequest
                    )
                }
                if(showAlertDialog){
                    AlertDialog(
                        onDismissRequest = { showAlertDialog = false },
                        confirmButton = {
                            Button(
                                onClick = {
                                    TasksSingleton.removeTask(task)
                                    taskRequest.taskRemoveRequest(task)
                                    showAlertDialog = false
                                }
                            ) {
                                Text(
                                    "Remover",
                                    color = MaterialTheme.colors.onPrimary
                                )
                            }
                        },
                        dismissButton = {
                            Button(
                                onClick = { showAlertDialog = false }
                            ) {
                                Text(
                                    "Cancelar",
                                    color = MaterialTheme.colors.onPrimary
                                )
                            }
                        },
                        title = {
                            Text(
                                "Remover tarefa?",
                                color = MaterialTheme.colors.onPrimary,
                                fontWeight = FontWeight.Bold
                            )
                        },
                        text = {
                            Text(
                                text = task.content,
                                color = MaterialTheme.colors.onPrimary
                            )
                        }
                    )
                }
            }
        }
    }
}

@Composable
fun DoneCheckbox(task: Task, modifier: Modifier = Modifier, taskRequest: TaskRequest){
    var checkedState by remember {mutableStateOf(false)}
    checkedState = task.isDone
    Checkbox(
        checked = checkedState,
        onCheckedChange = {
            checkedState = !checkedState
            task.isDone = checkedState
            taskRequest.taskUpdateRequest(task)
        },
        colors = CheckboxDefaults.colors(
            checkedColor = Green,
            uncheckedColor = MaterialTheme.colors.onPrimary,
            checkmarkColor = WhiteAccent
        ),
        modifier = modifier.size(10.dp)
    )
}