package com.nathan.todo_list_pdm.ui.components

import androidx.compose.material.AlertDialog
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.text.font.FontWeight
import com.nathan.todo_list_pdm.model.Task

@Composable
fun RemoveAlertDialog(task: Task){
    AlertDialog(
        onDismissRequest = { /*TODO*/ },
        confirmButton = {
            TextButton(
                onClick = { /*TODO*/ }
            ) {
                Text(
                    "Remover",
                    color = MaterialTheme.colors.onPrimary
                )
            }
        },
        dismissButton = {
            TextButton(
                onClick = { /*TODO*/ }
            ) {
                Text(
                    "Cancelar",
                    color = MaterialTheme.colors.onPrimary
                )
            }
        },
        title = {
            Text(
                "Remover tarefa?",
                color = MaterialTheme.colors.onPrimary,
                fontWeight = FontWeight.Bold
            )
        },
        text = {
            Text(
                text = task.content,
                color = MaterialTheme.colors.onPrimary
            )
        }
    )
}