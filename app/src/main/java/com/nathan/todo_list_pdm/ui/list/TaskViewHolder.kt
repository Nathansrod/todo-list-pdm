package com.nathan.todo_list_pdm.ui.list

import android.view.View
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nathan.todo_list_pdm.R
import com.nathan.todo_list_pdm.model.Task

class TaskViewHolder (
    itemView: View,
    private val adapter: TaskAdapter
): RecyclerView.ViewHolder(itemView) {
    private val etxtContent: TextView = itemView.findViewById(R.id.etxtContent)
    private val cbIsDone: CheckBox = itemView.findViewById(R.id.cbIsDone)
    private lateinit var currentTask: Task
    //Inicializando o listener

    init {
        cbIsDone.setOnClickListener {
            this.adapter.getOnClickDoneListener()?.OnClickDone(this.currentTask)
        }
    }
    fun bind(task: Task) {
        this.currentTask = task
        this.etxtContent.text = this.currentTask.content
    }
}