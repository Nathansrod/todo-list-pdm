package com.nathan.todo_list_pdm.ui.screens

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.nathan.todo_list_pdm.data.TaskRequest
import com.nathan.todo_list_pdm.data.TasksSingleton
import com.nathan.todo_list_pdm.ui.components.TaskInputField
import com.nathan.todo_list_pdm.ui.components.TaskItemView


@Composable
fun TaskListScreen(taskRequest: TaskRequest){
    Column(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        Surface(
            color = MaterialTheme.colors.background,
            modifier = Modifier.weight(1f, true)
        ){
            LazyColumn(
                modifier = Modifier.padding(10.dp)
            ){
                items(TasksSingleton.getTasks()) { task ->
                    TaskItemView(task = task, taskRequest = taskRequest)
                }
            }
        }
        Surface(
            color = MaterialTheme.colors.background,
            modifier = Modifier
                .height(intrinsicSize = IntrinsicSize.Max)
        ){
            TaskInputField(taskRequest)
        }
    }
}